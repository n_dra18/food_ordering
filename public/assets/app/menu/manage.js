$(document).ready(function(){

	var path_manage = '/menus';
	/**
	 * This initial icheck box in manage page
	 * 
	 */
	$('th>input').iCheck({
		checkboxClass: 'icheckbox_square-blue select_mass',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%'
	});

	$('td>input').iCheck({
		checkboxClass: 'icheckbox_square-blue do_disable_selected',
		radioClass: 'iradio_square-blue',
		increaseArea: '20%'
	});
	/*end icheckbox js*/

	/**
	 * This for handle event enable per record
	 * 
	 */
	$('.enable').click(function(){
		var id = $(this).data('id');
		

		$.get(base_url+path_manage+'/enable/'+id, function(data) {
			if (data.status == 'failed') {
				alert(data.message);
			} else {
				$('#can_order-'+id).html('Enable');
				$('#can_order-'+id).removeClass('label-warning');
				$('#can_order-'+id).addClass('label-success');
				alert(data.message);
			};
		});
	});

	/**
	 * This for hendle event disable per record
	 * 
	 */
	$('.disable').click(function(){
		var id = $(this).data('id');
		
		$.get(base_url+path_manage+'/disable/'+id, function(data) {
			if (data.status == 'failed') {
				
				alert(data.message);
				
			} else {
				$('#can_order-'+id).html('Disable');
				$('#can_order-'+id).removeClass('label-success');
				$('#can_order-'+id).addClass('label-warning');
				alert(data.message);
			};
		});
	});	
});

