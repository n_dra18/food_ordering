-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 25, 2015 at 05:44 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `food_ordering`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_pesanan`
--

CREATE TABLE IF NOT EXISTS `detail_pesanan` (
  `id_pemesanan` int(10) unsigned NOT NULL,
  `id_menu` int(10) unsigned NOT NULL,
  `jumlah` int(11) NOT NULL,
  `subtotal_pemesanan` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `id_pemesanan` (`id_pemesanan`,`id_menu`),
  KEY `id_menu` (`id_menu`),
  KEY `id_pemesanan_2` (`id_pemesanan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `meja`
--

CREATE TABLE IF NOT EXISTS `meja` (
  `id_meja` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quota` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_meja`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
  `id_menus` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nama_menus` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `base_photo` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_menus`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `pesanan`
--

CREATE TABLE IF NOT EXISTS `pesanan` (
  `id_pemesanan` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int(10) unsigned NOT NULL,
  `id_meja` int(10) unsigned NOT NULL,
  `nama_pemesan` varchar(30) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_pemesanan`),
  KEY `id_pemesanan` (`id_pemesanan`),
  KEY `id_meja` (`id_meja`),
  KEY `id_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE IF NOT EXISTS `transaksi` (
  `id_transaksi` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_pesanan` int(10) unsigned NOT NULL,
  `tgl_transaksi` date NOT NULL,
  `subtotal_transaksi` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_transaksi`),
  KEY `id_pesanan` (`id_pesanan`),
  KEY `id_pesanan_2` (`id_pesanan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `base_photo` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_user` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `role` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `remember_token` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `base_photo`, `nama_user`, `username`, `password`, `role`, `created_at`, `updated_at`, `remember_token`) VALUES
(19, 'yossiesam.png', 'Sam Fajar Muharram', 'yossiesam', 'eyJpdiI6ImJjUkM2Y1JYSFFlMElwSUZWQ1ZHNnc9PSIsInZhbHVlIjoiWDVPSmJPRzdGZjRFdGdsbmlZK09QUT09IiwibWFjIjoiYWFlMDhiM2Q1YWIzMjY5ZWNhMDQ0YTY0OGNkOTU0ZmQxNDExOTg0OWZlNGM3ZmM4N2Y0ZWRlMjA4MjBhNzU4YiJ9', 3, '2015-04-11 13:47:56', '2015-04-13 09:54:54', NULL),
(20, 'yossie.png', 'Yossie', 'yossie', 'eyJpdiI6IjJQNjRMbEszcUFERFdSd1wvdDEwMVl3PT0iLCJ2YWx1ZSI6Ink4d2Y4SDV1NDRuNkVzYmcxdUxCQ0E9PSIsIm1hYyI6IjNjZTM5MTc3ODU4NmNiZDllMjVlZWMxMTA1NTMzNzJjY2JiZWZmYzY4NGQ1YWZjYzYyNGE1NjdkMmVjZGZiZmEifQ==', 3, '2015-04-11 22:15:17', '2015-04-11 22:15:17', NULL),
(21, NULL, 'asdad', 'asdasd', 'eyJpdiI6ImtXZ0xjaUQ0czVxQkVXQktlQVFBd2c9PSIsInZhbHVlIjoidlEyUkFyVFFcL3U2YkJ5M1ZLczBvN3c9PSIsIm1hYyI6IjU5YjU2MjMzY2FkNzViOTIyMGQ4MmU2OGJhN2IxMTkxZDZhYjAzZTQzOTc3OTg3ZTZjZGZmZWVhMzhiZjcxNjUifQ==', 2, '2015-04-11 22:16:48', '2015-04-11 22:16:48', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_pesanan`
--
ALTER TABLE `detail_pesanan`
  ADD CONSTRAINT `detail_pesanan_ibfk_1` FOREIGN KEY (`id_menu`) REFERENCES `menus` (`id_menus`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `detail_pesanan_ibfk_2` FOREIGN KEY (`id_pemesanan`) REFERENCES `pesanan` (`id_pemesanan`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pesanan`
--
ALTER TABLE `pesanan`
  ADD CONSTRAINT `pesanan_ibfk_2` FOREIGN KEY (`id_meja`) REFERENCES `meja` (`id_meja`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `pesanan_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`id_pesanan`) REFERENCES `pesanan` (`id_pemesanan`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
