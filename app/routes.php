<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(array('namespace' => 'FoodOrdering'), function(){

	Route::get('/login', function(){
		return View::make('content.login');
	});
	Route::post('/login', 'UserController@login');
	Route::get('/logout', 'UserController@logout');


	Route::get('/', array(
		'uses' => 'HomeController@dashboard', 
		'before' => array('auth', 'landing_page'), 
		'as' => '/')
	);

	/**
	* Route Group Menus
	*/
	Route::group(array('prefix' => 'menus', 'before' => array('auth', 'menus')), function() {

		/*This Route to menu manage*/
		Route::get('/manage', array(
			'as' => 'menus_manage', 
			'uses'=> 'MenusController@manage')
		);

		/*Route to ada the menus*/
		Route::get('/add', array(
			'as' => 'get_add_menu',
			'uses' => 'MenusController@getAdd',
		));
		Route::post('/add', array(
			'as' => 'post_add_menu',
			'uses' => 'MenusController@postAdd',
		));			

		/**
		* This rout Group to edit menus
		*/
		Route::group(array('prefix' => 'edit'), function() {
			
			/*Route to get edit page*/
			Route::get('/{id}', array(
				'uses' => 'MenusController@getEdit',
				'as' => 'get_edit_menu',
				));
			/*Route to post edit page*/
			Route::post('/{id}', array(
				'uses' => 'MenusController@postEdit',
				'as' => 'post_edit_menu'
				));

		});

		/*Route to delete the menus*/
		Route::get('/delete/{id}', array(
			'uses' => 'MenusController@delete',
			'as' => 'delete_menu'
			));

		Route::get('/enable/{id}', array(
			'uses' => 'MenusController@enable',
			'as' => 'enable_menu',
			));

		Route::get('/disable/{id}', array(
			'uses' => 'MenusController@disable',
			'as' => 'disable_menu',
			));		

	});

	/**
	* Route Group Users
	*/
	Route::group(array('prefix' => 'users', 'before' => array('auth', 'users')), function() {
		
		/*Route manage user*/
		Route::get('/manage', array(
			'as' => 'user_manage', 
			'uses'=> 'UserController@manage')
		);

		/**
		* Route Group edit user
		*/
		Route::group(array('prefix' => 'edit'), function() {
			/*Route to get user data*/
			Route::get('/{id}', 'UserController@edit');
			/*Route to post user data*/
			Route::post('/{id}', 'UserController@processEdit');
		});

		/*Route for user delete*/
		Route::get('/delete/{id}', 'UserController@delete');
		/*Route for handle user add*/
		Route::post('/add', 'UserController@add');
	});


	/**
	* Route Group Order
	*/
	Route::group(array('prefix' => 'order', 'before' => array('auth', 'order')), function() {

		/*Route manage order*/
		Route::get('/', array(
			'as' => 'order', 
			'uses'=> 'OrderController@getOrder')
		);
		/*Route manage order*/
		Route::post('/', 'OrderController@proccessOrder');
	});

	/**
	* Route Group ajax
	*/
	Route::group(array('prefix' => 'ajax', 'before' => 'auth'), function() {

		/*Route manage order*/
		Route::get('/getallmenus', array(
			'as' => 'get_ajax_all_menus', 
			'uses'=> 'AjaxController@getAllMenus')
		);
		
		/*Route manage order*/
		Route::post('/getallmenus', array(
			'as' => 'post_ajax_all_menus', 
			'uses'=> 'AjaxController@getAllMenus')
		);

		/*Route manage order*/
		Route::get('/availableTable', array(
			'as' => '/availableTable', 
			'uses'=> 'AjaxController@availableTable')
		);
	});

	/**
	* Route Group Kitchen
	*/
	Route::group(array('prefix' => 'kitchen', 'before' => array('auth', 'kitchen')), function() {

		/*Route manage kitchen*/
		Route::get('/', array(
			'as' => 'kitchen', 
			'uses'=> 'KitchenController@getKitchen')
		);

		Route::get('/apply/{id}','KitchenController@apply');
	});

	/**
	* Route Group Transaction
	*/
	Route::group(array('prefix' => 'transaction', 'before' => array('auth', 'transaction')), function() {

		/*Route manage transaction*/
		Route::get('/', array(
			'as' => 'transaction', 
			'uses'=> 'TransactionController@manage')
		);

		Route::get('/detail', array(
			'as' => 'detail/transaction', 
			'uses'=> 'AjaxController@getDetailTransaction')
		);
		Route::post('/finished', array(
			'as' => 'detail/transaction', 
			'uses'=> 'TransactionController@finished')
		);		
	});

	/**
	* Route maxdb_report(flags)
	*/
	Route::group(array('before' => array('auth', 'report')), function() {

		Route::get('/report', array(
			'as' => 'report', 
			'uses'=> 'ReportController@getReport'
			));

		Route::post('/report', array(
			'as' => 'reportpost', 
			'uses'=> 'ReportController@getReport'
			));	
	});

	/**
	* Route Group Transaction
	*/
	Route::group(array('prefix' => 'settings', 'before' => array('auth', 'setting')), function() {

		/*Route manage transaction*/
		Route::get('/', array(
			'as' => 'setting', 
			'uses'=> 'SettingController@index')
		);

		/*Route manage transaction*/
		Route::post('/', array(
			'as' => 'saveSetting', 
			'uses'=> 'SettingController@index')
		);		
	});
});