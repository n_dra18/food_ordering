<!DOCTYPE html>
<html lang="en">
@include ('layout.head')
<body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      @include ('layout.header')
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      @include ('layout.sidebar-left')
      <!--sidebar end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      @yield ('content')
      <!--main content end-->

      <!--footer start-->
      @include ('layout.footer')
      <!--footer end-->
      
    </section>
    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{ URL::asset('assets/js/jquery.js') }}"></script>
    <script src="{{ URL::asset('assets/js/jquery-1.8.3.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/jquery-ui-1.9.2.custom.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/jquery.ui.touch-punch.min.js') }}"></script>
    <script class="include" type="text/javascript" src="{{ URL::asset('assets/js/jquery.dcjqaccordion.2.7.js') }}"></script>
    <script src="{{ URL::asset('assets/js/jquery.scrollTo.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/jquery.nicescroll.js') }}" type="text/javascript"></script>

    <!--common script for all pages-->
    <script src="{{ URL::asset('assets/js/common-scripts.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('assets/js/gritter/js/jquery.gritter.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/gritter-conf.js') }}"></script>

    <!--script for this page-->   
    <script src="{{ URL::asset('assets/js/zabuto_calendar.js') }}"></script>
    <script src="{{ URL::asset('assets/js/sparkline-chart.js') }}"></script>     
    
  </body>
  </html>
