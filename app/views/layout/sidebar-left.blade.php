<!--sidebar start-->
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
        
        	  <p class="centered"><a href="{{URL::to('/')}}"><img src="{{URL::asset('assets/img/logo.jpg')}}" class="img-circle" width="60"></a></p>
        	  <h5 class="centered">Food Ordering</h5>
        	
            @if(Auth::user()->ability('SuperAdmin', 'manage_all'))
            <li class="mt">
                <a href="{{ URL::to('/')}}">
                    <i class="fa fa-dashboard"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            @endif

            @if(Auth::user()->ability('SuperAdmin', 'manage_all,manage_menus'))
            <li class="sub-menu">
                <a href="{{ URL::to('/menus/manage')}}">
                    <i class="fa fa-cutlery"></i>
                    <span>Manage Menus</span>
                </a>
            </li>
            @endif

            @if(Auth::user()->ability('SuperAdmin', 'manage_all,manage_user'))
            <li class="sub-menu">
                <a href="{{ URL::to('/users/manage')}}">
                    <i class="fa fa-users"></i>
                    <span>Manage Users</span>
                </a>
            </li>
            @endif

            @if(Auth::user()->ability('SuperAdmin', 'manage_all,manage_transaction'))
            <li class="sub-menu">
                <a href="{{ URL::to('/transaction')}}">
                    <i class="fa fa-usd"></i>
                    <span>Manage Transactions</span>
                </a>
            </li>            
            @endif

            @if(Auth::user()->ability('SuperAdmin', 'manage_all'))
            <li class="sub-menu">
                <a href="{{ URL::to('/kitchen')}}">
                    <i class="fa fa-spoon"></i>
                    <span>View Kitchen</span>
                </a>
            </li>
            @endif

            @if(Auth::user()->ability('SuperAdmin', 'manage_all,manage_report'))
            <li class="sub-menu">
                <a href="{{ URL::to('/report')}}">
                    <i class="fa fa-book"></i>
                    <span>Report</span>
                </a>
            </li>
            @endif

            @if(Auth::user()->ability('Writers', 'manage_order'))
            <li class="sub-menu">
                <a href="{{ URL::to('/order')}}">
                    <i class="fa fa-book"></i>
                    <span>Do Order</span>
                </a>
            </li>
            @endif

            @if(Auth::user()->ability('SuperAdmin', 'manage_all'))
            <li class="sub-menu">
                <a href="{{ URL::to('/settings')}}">
                    <i class="fa fa-cog"></i>
                    <span>Settings</span>
                </a>
            </li>
            @endif                         
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->
