<!-- **********************************************************************************************************************************************************
RIGHT SIDEBAR CONTENT
*********************************************************************************************************************************************************** -->                  

<div class="col-lg-3 ds">
  <!--COMPLETED ACTIONS DONUTS CHART-->
  <h3>NOTIFICATIONS ORDERS</h3>

<!-- First Action -->
@foreach($notif as $notif)
<div class="desc">
  <div class="thumb">
    <img src="{{ URL::asset('assets/img/img_table')}}/{{$notif->id_meja}}.png" class="img-circle img-responsive">
  </div>
  <div class="details">
    <p><muted>2 Minutes Ago</muted><br/>
    {{$notif->nama_pemesan}} - 
    @if ($notif->status=='1') Still Waiting Menus
    @elseif ($notif->status=='2') Finished and Paid
    @else Still Eat
    @endif
   </p>
  </div>
</div>
@endforeach

<!-- USERS ONLINE SECTION -->
<h3>USER ONLINE</h3>
<!-- First Member -->
<div class="desc">
  <div class="thumb">
    <img class="img-circle" src="{{ URL::asset('assets/img/ui-divya.jpg') }}" width="35px" height="35px" align="">
  </div>
  <div class="details">
    <p><a href="#">DIVYA MANIAN</a><br/>
     <muted>Available</muted>
   </p>
  </div>
</div>
</div>
<!-- end col-lg-3 -->