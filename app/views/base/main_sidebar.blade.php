<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
  <!-- Sidebar user panel -->
  <div class="user-panel">
    <div class="pull-left image">
      @if(Auth::user()->base_photo)
      <img src="{{ URL::asset('uploads') }}/{{Auth::user()->base_photo}}" class="img-circle" alt="User Image">
      @else
      <img src="{{ URL::asset('uploads') }}/users/no_image.jpg" class="img-circle" alt="User Image">
      @endif
    </div>
    <div class="pull-left info">
      <p>{{ Auth::user()->nama_user }}</p>
      <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
  </div>
  <!-- search form -->
  <form action="#" method="get" class="sidebar-form">
    <div class="input-group">
      <input type="text" name="q" class="form-control" placeholder="Search...">
      <span class="input-group-btn">
        <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
      </span>
    </div>
  </form>
  <!-- /.search form -->
  <!-- sidebar menu: : style can be found in sidebar.less -->
  <ul class="sidebar-menu">
    <li class="header">MAIN NAVIGATION</li>
    <li class="@if(Route::is('/')) active @endif)">
      <a href="{{ URL::route('/') }}">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
      </a>
    </li>
    <li class="treeview
      @if(Route::getCurrentRoute()->getPrefix() == '/menus' || Route::getCurrentRoute()->getPrefix() == 'menus/edit')
        active
      @endif">
      <a href="#">
        <i class="fa fa-folder"></i>
        <span>Master</span>
        <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a href="{{ URL::route('menus_manage') }}"><i class="fa fa-list"></i> Menu</a></li>
        <li><a href="pages/UI/icons.html"><i class="fa fa-users"></i> User</a></li>
      </ul>              
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-money"></i>
        <span>Transaction</span>
        <i class="fa fa-angle-left pull-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a href="pages/UI/general.html"><i class="fa fa-credit-card"></i>Chasier</a></li>
        <li><a href="pages/UI/icons.html"><i class="fa fa-line-chart"></i>Report</a></li>
      </ul>                  
    </li>            
    <li>
      <a href="#">
        <i class="fa fa-cutlery"></i>
        <span>Kitchen</span>
      </a>
    </li>
    <li>
      <a href="#">
        <i class="fa fa-wrench"></i>
        <span>Settings</span>
      </a>
    </li>                                     
  </ul>
</section>
<!-- /.sidebar -->