<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
        <title>Food Ordering | Dashboard Kitchen</title>
        <!-- BOOTSTRAP CORE STYLE CSS -->
        <link href="{{ URL::asset('assets/css/bootstrap.css') }}" rel="stylesheet" />
        <!-- FONTAWESOME STYLE CSS -->
        <link href="{{ URL::asset('assets/css/font-awesome.min.css') }}" rel="stylesheet" />
        <!-- PRETTYPHOTO STYLE CSS -->
        <link href="{{ URL::asset('assets/css/prettyPhoto.css')}}" rel="stylesheet" />
        <!-- CUSTOM STYLE CSS -->
        <link href="{{ URL::asset('assets/css/style.css')}}" rel="stylesheet" />    
        <!-- GOOGLE FONT -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

        <!-- CORE JQUERY  -->
        <script src="{{ URL::asset('assets/plugins/jquery-1.10.2.js')}}"></script>
        <!-- BOOTSTRAP SCRIPTS  -->
        <script src="{{ URL::asset('assets/plugins/bootstrap.js')}}"></script>
        <!-- PRETTYPHOTO SCRIPTS  -->
        <script src="{{ URL::asset('assets/plugins/jquery.prettyPhoto.js')}}"></script>
        <!-- CUSTOM SCRIPTS  -->
        <script src="{{ URL::asset('assets/js/custom.js')}}"></script>
  <!--/.HEADING END-->

</head>