@include('kitchen.layout.head')

@include('kitchen.layout.header')
<section class="wrapper">
    @yield('kitchen.content')
</section>
     <!--/. END PORTFOLIO SECTION-->