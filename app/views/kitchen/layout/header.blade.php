    <div class="navbar navbar-inverse navbar-fixed-top" >
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                </button>
                <a class="navbar-brand" href="{{ URL::to('/') }}">Dashboard Kitchen</a>

            </div>
            @if(Entrust::hasRole('SuperAdmin'))
            <a style="  float: right; margin-right: 50px; margin-top: 8px;" class="btn btn-primary" href="{{ URL::to('/') }}">Home</a>           
    		@endif
            @if(Entrust::hasRole('Chef'))
            <a style="  float: right; margin-right: 50px; margin-top: 8px;" class="btn btn-primary" href="{{ URL::to('/logout') }}">Logout</a>           
    		@endif    		
    </div>
   <!--/.NAVBAR END-->