@extends ('kitchen.layout.layout')

@section ('kitchen.content')
@foreach($orders as $order)
<div class="container">
  <div class="row placeholders">
    <div class="col-xs-6 col-sm-12 placeholder">
      <div class="content-panel">
        <h4><img src="{{ URL::asset('assets/img/img_table')}}/{{$order->table->id_meja}}.png" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail"></h4>
        <h4><i class="fa fa-angle-right"> Order by {{$order->nama_pemesan}}</i></h4>
        <hr>
        <table class="table">
          <thead>
            <tr>
              <th>No</th>
              <th>Name Menu</th>
              <th>Quantity</th>
            </tr>
          </thead>
          <tbody>
            @foreach($order->detailPesanan as $index => $detail)
            <tr>
              <td>{{$index+1}}</td>
              <td>{{$detail->menu->nama_menus}}</td>
              <td>{{$detail->jumlah}}</td>
            </tr>
            @endforeach
            <tr>
              <td>@if(Entrust::hasRole('Chef'))<a class="btn btn-primary" href="{{URL::to('kitchen/apply')}}/{{$order->id_pemesanan}}">Apply</a>@endif</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<br>
@endforeach
<script src="{{ URL::asset('/assets/js/socket.io.js') }}"></script>
<script>
  var socket = io.connect('192.168.0.104:8080');

  socket.on('recieve notif', function(msg){
    showNewMenus(msg);
  });

  function showNewMenus (msg) {
    console.log(msg);
    var order = '<div class="container">'+
    '<div class="row placeholders">'+
    '<div class="col-xs-6 col-sm-12 placeholder">'+
    '<div class="content-panel">'+
    '<h4><img src="http://192.168.0.104/food_ordering/public/assets/img/img_table/'+msg.table_number+'.png" class="center-block img-responsive img-circle" alt="Generic placeholder thumbnail"></h4>'+
    '<h4><i class="fa fa-angle-right">'+msg.behalf_of+'</i></h4>'+
    '<hr>'+
    '<table class="table">'+
    '<thead>'+
    '<tr>'+
    '<th>No</th>'+
    '<th>Name Menu</th>'+
    '<th>Quantity</th>'+
    '</tr>'+
    '</thead>'+
    '<tbody>';

    $.each(msg.menus, function(i, menu) {
        order = order+
        '<tr>'+
          '<td>'+(i+1)+'</td>'+
          '<td>'+menu.nama_menu+'</td>'+
          '<td>'+menu.qty+'</td>'+
        '</tr>';
    });

    order = order+'<tr>'+
      '<td><a class="btn btn-primary" href="http://192.168.0.104/food_ordering/public/kitchen/apply/6">Apply</a></td>'+
      '<td></td>'+
      '<td></td>'+
      '<td></td>'+
    '</tr>'+
    '</tbody>'+
    '</table>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div><br>';

    $('.wrapper').append(order);
  }

</script>
@stop
