@extends ('layout.layout')

@section ('content')

<!--main content start-->
<section id="main-content">
  <section class="wrapper">

    <div class="row">
      <div class="col-lg-9">
        <h3><i class="fa fa-angle-right"></i> Report</h3>
        <div class="content-panel" style="padding-top:5px !important; text-align:center">
          <h5>Choise Date of Transaction</h5>
          <form class="form-inline" role="form">
            <div class="form-group">
              <input type="text" class="form-control" name="start-date" placeholder="Start Date">
            </div>
            -
            <div class="form-group">
              <input type="text" class="form-control" name="end-date" placeholder="End Date">
            </div>
            <button type="submit" class="btn btn-theme">Find</button>
          </form>
          <br>
        </div><!-- /content-panel -->
        <div class="content-panel">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>#</th>
                <th>Date</th>
                <th>Table</th>
                <th>Name Customer</th>
                <th>Total Transaction</th>
              </tr>
            </thead>
            <tbody>
              @foreach($report as $index => $value)
              <tr>
                <td>{{$index+1}}</td>
                <td>{{$value->tgl_transaksi}}</td>
                <td>{{$value->pesanan->id_meja}}</td>
                <td>{{$value->pesanan->nama_pemesan}}</td>
                <td>{{$value->total}}</td>
              </tr>
              @endforeach
              <tr>
                <td colspan="4"></td>
                <td align="right"><a href="#" class="btn btn-theme">Add Menus</a></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      @include ('layout.sidebar-right')        
    </div>

  </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
<!--main content end-->
<script type="text/javascript">
  $('.form-control').datepicker();
</script>  
@stop