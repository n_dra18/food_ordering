@extends ('layout.layout')

@section ('content')

<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-9">
        <h3><i class="fa fa-angle-right"></i> Transactions</h3>
        <div class="content-panel">
          <h4><i class="fa fa-angle-right"></i> Daily Report Transactions</h4>
          <section id="unseen">
            <table class="table table-bordered table-striped table-condensed">
              <thead>
                <tr>
                  <th><i class="fa fa-th-large"></i> Table</th>
                  <th><i class="fa fa-user"></i> Name Customer</th>
                  <th><i class=" fa fa-edit"></i> Status</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                @foreach($orders as $order)
                <tr>
                  <td>{{$order->id_meja}}</td>
                  <td>{{$order->nama_pemesan}}</td>
                  @if($order->status==1)
                  <td><span class="label label-warning label-mini">On Proccess</span></td>
                  @else
                  <td><span class="label label-success label-mini">Delivered</span></td>
                  @endif
                  <td><button data-id_pesanan="{{$order->id_pemesanan}}" class=" detail btn btn-primary btn-xs" title="Click to Detail"><i class="fa fa-search-plus"></i></button></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </section>
        </div><!-- /content-panel -->
      </div>
      @include ('layout.sidebar-right')        
    </div>
  </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
<!--main content end-->

<!-- Loading Modal -->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="loadingModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <h4>Loading...</h4>
      </div>
    </div>
  </div>
</div>
<!-- Loading modal -->

<!-- Modal -->
<div aria-hidden="true" aria-labelledby="myModal" role="dialog" tabindex="-1" id="myModal" class="modal fade">
  <div class="modal-dialog">
    <form action="{{ URL::to('transaction/finished') }}" method="POST">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Detail Transaction</h4>
        </div>
        <div class="modal-body">
          <input id="id_transaction" type="hidden" name="id_transaction">
          <div class="form-panel">
            <!-- main modal -->
            <h4>Responsive Table</h4>
            <section id="unseen">
              <table id="detail_pesanan" class="table table-bordered table-striped table-condensed">
                <thead>
                  <tr>
                    <th>Name Menu</th>
                    <th class="numeric">Price</th>
                    <th class="numeric">Quantity</th>
                    <th class="numeric">Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                  <tr>
                    <td style="text-align:right;" colspan="3">Subtotal</td>
                    <td id="subtotal" class="numeric">-0.36%</td>
                  </tr>
                  <tr>
                    <td style="text-align:right;" colspan="3">Service & Tax</td>
                    <td id="taxandservice" class="numeric">-0.36%</td>
                  </tr>
                  <tr>
                    <td style="text-align:right;" colspan="3">Total</td>
                    <td id="total" class="numeric">-0.36%</td>
                  </tr>
                </tfoot>
              </table>
            </section>
          </div>
        </div>
        <div class="modal-footer">
          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
          <button class="btn btn-theme" type="submit">Print and Finished</button>
        </div>      
      </div>
    </form>
  </div>
</div>

<script>
  $(document).ready(function(){

    $(".detail").click(function(){

      var id_pemesanan = $(this).attr('data-id_pesanan')

      $.ajax({
        url: "{{URL::to('/transaction/detail')}}",
        data : { id_pemesanan : id_pemesanan},
        method: "GET",         
        success: function( data ) {
          data = jQuery.parseJSON(data);
          $('#detail_pesanan > tbody > tr').remove();
          $.each( data.order.detail_pesanan, function( key, value ) {
            $('#detail_pesanan > tbody:last').append('<tr><td>'+
              value.menu.nama_menus+'</td><td class="numeric">'+
              value.menu.harga+'</td><td class="numeric">'+
              value.jumlah+'</td><td class="numeric">'+
              parseInt(value.jumlah*value.menu.harga)+'</td></tr>');
          });
          var load = $('#loadingModal').modal('show');
          clearTimeout(load.data('hideInterval'));
          load.data('hideInterval', setTimeout(function(){
            load.modal('hide');
            $('#subtotal').html(data.price.subtotal);
            $('#taxandservice').html(data.price.taxandservice);
            $('#total').html(data.price.total);
            $('#id_transaction').val(id_pemesanan);
            $('#myModal').modal('show');
          }, 3000));
        }
      });
});
});

</script>
<!-- modal -->   
@stop