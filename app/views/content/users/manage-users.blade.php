@extends ('layout.layout')

@section ('content')
<section id="main-content">
  <section class="wrapper">
    <div class="row">
      <div class="col-lg-9">   
        <h3 style="float: left; margin-right: 65%;"><i class="fa fa-angle-right"></i> Manage Users</h3>
        <h3><a data-toggle="modal" id="add" href="#" class="btn btn-theme02">Add Users</a></h3>
        @if($messageOtherFunction)
          @if($messageOtherFunction['status']=='success')
            <div class="alert alert-success" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <a href="#" class="alert-link">{{$messageOtherFunction['message']}}</a>
            </div>
          @else
            <div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <a href="#" class="alert-link">{{$messageOtherFunction['message']}}</a>
            </div>   
          @endif  
        @endif


        <!-- Modal -->
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add User</h4>
              </div>

              <div class="modal-body">
                <div class="form-panel">
                  <form id="add_user" class="form-horizontal style-form" enctype="multipart/form-data" action="{{URL::to('users/add')}}" method="post">
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label">Role</label>
                      <div class="col-sm-10">
                        <select required name="role">
                          <option value=""></option>
                          <option value="1">Super Admin</option>
                          <option value="2">Cashier</option>
                          <option value="3">Writers</option>
                          <option value="4">Chef</option>
                        </select>                
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label">Name of User</label>
                      <div class="col-sm-10">
                        <input required id="name" type="text" class="form-control" name="name" data-validate="required" placeholder="name of user">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label">User Name</label>
                      <div class="col-sm-10">
                        <input required type="text" class="form-control" name="user_name" placeholder="user name">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label">Password</label>
                      <div class="col-sm-10">
                        <input required id="password" type="password" class="form-control" name="pass" placeholder="password">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label">Confrim Password</label>
                      <div class="col-sm-10">
                        <input id="cnf_password" required type="password" class="form-control" name="cnf_pass" placeholder="confrim password">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-2 col-sm-2 control-label" for="exampleInputFile">Upload Photo</label>
                      <div class="col-sm-10">
                        <input class="required" accept="image/*" name="photo" type="file" id="exampleInputFile">
                        <p class="help-block">Format image must JPG or PNG</p>
                      </div>            
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                  <button class="btn btn-theme submit" type="submit">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- modal -->
        
        <div class="row mt">
          <!-- SERVER STATUS PANELS -->
          @foreach($users as $user)
          <div class="col-md-4 col-sm-4 mb">
            <div class="white-panel pn">
              <div class="white-header">
                <h5>{{$user->nama_user}} -
                  @if($user->role==1)
                  Super Admin
                  @elseif ($user->role==2)
                  Cashier
                  @elseif ($user->role==3)
                  Writers
                  @else
                  Chef
                  @endif
                </h5>
              </div>
              <div class="row">
                <div class="col-sm-6 col-xs-6"></div>
              </div>
              <div class="centered" style="margin-bottom:30px; min-height:120px;">
                @if($user->base_photo)
                <img src="{{ URL::asset('uploads') }}/{{$user->base_photo}}" width="120">
                @else
                <img src="{{ URL::asset('assets/img/no_photo.jpg') }}" width="120">
                @endif
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="btn-group">
                    <button type="button" class="btn btn-theme">Action</button>
                    <button type="button" class="btn btn-theme dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a class="edit-user" href="#" user-id="{{$user->id}}">Edit</a></li>
                      <li><a href="{{URL::to('/users/delete')}}/{{ $user->id }}">Delete</a></li>
                    </ul>
                  </div>
                </div>
              </div>              
            </div>
          </div><!-- /col-md-4 -->

          <!-- Modal -->
          <div aria-hidden="true" aria-labelledby="myModalEdit" role="dialog" tabindex="-1" id="myModalEdit" class="modal fade">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title">Edit User</h4>
                </div>
                <div class="modal-body">
                  <div class="form-panel">
                    <form id="edit_user" class="form-horizontal style-form" enctype="multipart/form-data" method="post">
                      <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Role</label>
                        <div class="col-sm-10">
                          <select name="role">
                            <option value="1">Super Admin</option>
                            <option value="2">Cashier</option>
                            <option value="3">Writers</option>
                            <option value="4">Chef</option>
                          </select>                
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Name of User</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="name" data-validate="required" placeholder="name of user">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">User Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="user_name" placeholder="user name">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                          <input type="text" id="edit_pass" class="form-control" name="pass" placeholder="password">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label">Confrim Password</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="cnf_pass" placeholder="confrim password">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 col-sm-2 control-label" for="exampleInputFile">Upload Photo</label>
                        <div class="col-sm-10">
                          <input name="photo" type="file">
                          <p class="help-block">Format image must JPG or PNG</p>
                          <img id="img" width="120">
                        </div>          
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                    <button class="btn btn-theme" type="submit">Submit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <!-- modal -->
          <!-- Loading Modal -->
          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="loadingModal" class="modal fade">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">
                  <h4>Loading...</h4>
                </div>
              </div>
            </div>
          </div>
          <!-- end loading modal -->       

          @endforeach
        </div>
      </div><!-- /col-lg-9 END SECTION MIDDLE -->
      @include ('layout.sidebar-right')        
    </div>

  </section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
<!--main content end-->


<!-- this function validator of add user -->
<!-- start validator add user -->
<script type="text/javascript">
  $("#add_user").validate({
    rules: {
      pass: {
        required: true,
        minlength: 5            
      },
      cnf_pass: {
        required: true,
        minlength: 5,
        equalTo: "#password"
      },         
    }
  });  
</script>
<!-- end validator add user -->

<!-- this function validator of edit user -->
<!-- start validator edit user -->
<script type="text/javascript">
  $("#edit_user").validate({
    rules: {
      pass: {
        minlength: 5            
      },
      cnf_pass: {
        minlength: 5,
        equalTo: "#edit_pass"
      },         
    }
  });  
</script>
<!-- end validator edit user -->
@stop