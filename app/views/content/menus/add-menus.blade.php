@extends ('base.layout')

@section ('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Menu
    <small>Add your menu here.</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
    <li>Menu</li>
    <li class="active">Add</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
    @if(Session::has('success'))
      <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {{ Session::get('success') }}.
      </div>
      @endif        
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Form</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" method="POST" action="" enctype="multipart/form-data">
          <div class="box-body">
            @if($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <strong>Opppsss!</strong> {{ implode('', $errors->all('<li>:message</li>')) }}
            </div>
            @endif             
            <div class="form-group" data-toggle="tooltip" data-placement="bottom" title="Enter your menu name like Nasi Goreng Pedas or Ramen. (Must be filled)">
              <label for="menuname">Menu name *</label>
              <input name="menuname" type="text" class="form-control" id="menuname">
            </div>
            <div class="form-group"  data-toggle="tooltip" data-placement="bottom" title="Enter your menu price. (Must be filled)">
              <label for="menu-price">Price *</label>
              <input name="price" type="text" class="form-control price-input" id="menu-price">
            </div>
            <div class="form-group" data-toggle="tooltip" data-placement="bottom" title="Enter your menu stock per day (Must be filled)">
              <label for="menu-stock">Stock *</label>
              <input name="stock" type="number" class="form-control" id="menu-stock">
            </div>
            <div class="form-group" data-toggle="tooltip" data-placement="bottom" title="Enter your menu stock per day (Must be filled)">
              <label for="menu-keterangan">Description</label>
              <textarea name="keterangan" class="form-control" id="menu-keterangan"></textarea>
            </div>                                
            <div class="form-group"  data-toggle="tooltip" data-placement="bottom" title="Enter your menu picture, this is optoinal input">
              <label for="exampleInputFile">Image</label>
              <input name="image" type="file" id="exampleInputFile">
              <p class="help-block">Format allowed is JPG, PNG. And size not allowed more than 1 Mega Bytes.</p>
            </div>
            <div class="checkbox">
              <label>
                <input name="can_order" value="1" type="checkbox"> Can Order Now ?
              </label>
            </div>
          </div><!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" name="add" value="add" class="btn bg-green btn-flat">Add and Close</button>
            <button type="submit" name="add_more" value="add_more" class="btn bg-blue btn-flat">Add and Add More</button>
            <a href="{{ URL::route('menus_manage') }}" class="btn bg-red btn-flat">Cancel</a>
          </div>
        </form>
      </div><!-- /.box -->
    </div>
    <div class="col-md-6">
      <!-- Horizontal Form -->
      <div class="box box-info" data-toggle="tooltip" data-placement="bottom" title="Picture will be show in android application.">
        <div class="box-header with-border">
          <h3 class="box-title">Picture</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal">
          <div class="box-body">
            <img id="menu_picture" src="../assets/images/product_image_not_available.gif">
          </div><!-- /.box-body -->
        </form>
      </div><!-- /.box -->
    </div>                       
  </div>
</section><!-- /.content -->
@stop