@extends ('base.layout')

@section ('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Menu
    <small>Add your menu here.</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Master</a></li>
    <li>Menu</li>
    <li class="active">Add</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      @if($errors->any())
      <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Opppsss!</strong> {{$errors->first()}}.
      </div>
      @endif
      @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong>Success!</strong> {{ Session::get('success') }}.
        </div>
        @endif             
      <!-- Application buttons -->
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Application Buttons</h3>
        </div>
        <div class="box-body">
          <a href="{{ URL::route('get_add_menu') }}" class="btn btn-app">
            <i class="fa fa-plus"></i> Add
          </a>           
          <a href="#" id="do_disable_selected" class="btn btn-app">
            <i class="fa fa-lock"></i> Disable Selected
          </a>
          <a class="btn btn-app">
            <i class="fa fa-unlock-alt"></i> Enable Selected
          </a>
          <a class="btn btn-app">
            <i class="fa fa-trash"></i> Delete Selected
          </a>
          <a class="btn btn-app">
            <i class="fa fa-filter"></i> Filter
          </a>                  
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>            
  </div>
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Responsive Hover Table</h3>
          <div class="box-tools">
            <div class="input-group" style="width: 150px;">
              <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
              <div class="input-group-btn">
                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
              </div>
            </div>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body no-padding">
          <table id="tabel_menu" class="table table-hover">
            <tbody>
            <tr>
              <th><input type="checkbox" name="select_mass" id="select_mass"></th>
              <th>ID</th>
              <th>Picture</th>
              <th>Name</th>
              <th>Price</th>
              <th>Status</th>
              <th>Description</th>
              <th>Action</th>
            </tr>
            @foreach($menus as $menu)
            <tr>
              <td><input type="checkbox" id="menu-{{ $menu->id_menus }}" class="select_mass"></td>
              <td>{{ $menu->id_menus }}</td>
              <td>
              @if($menu->base_photo)
                <img width="60px" src="{{ URL::asset('uploads/menus') }}/{{ $menu->getSlug() }}/{{ $menu->getSlug() }}-small.jpg">
              @endif 
              </td>
              <td>{{ $menu->nama_menus }}</td>
              <td>{{ $menu->harga }}</td>
              <td>
                @if($menu->can_order)
                  <span id="can_order-{{$menu->id_menus}}" class="label label-success">Enable</span>
                @else
                  <span id="can_order-{{$menu->id_menus}}" class="label label-warning">Disable</span>
                @endif
              </td>
              <td>{{ strlen($menu->keterangan) >= 60 ? substr($menu->keterangan, 0, 120).'...' : $menu->keterangan }}</td>
              <td>
                <div class="btn-group">
                  <button type="button" class="pull-right btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    <li><a href="{{ URL::route('get_edit_menu', $menu->id_menus) }}"><i class="fa fa-pencil-square"></i>Edit</a></li>
                    <li><a href="{{ URL::route('delete_menu', $menu->id_menus) }}"><i class="fa fa-trash"></i>Delete</a></li>
                    <li class="divider"></li>
                    <li><a class="enable" href="#" data-id="{{ $menu->id_menus }}"><i class="fa fa-lock"></i> Enable</a></li>
                    <li><a class="disable" href="#" data-id="{{ $menu->id_menus }}"><i class="fa fa-unlock-alt"></i> Disable</a></li>
                  </ul>
                </div>                        
              </td>
            </tr>
            @endforeach                                                                                                                      
          </tbody>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
</section><!-- /.content -->
<script src="{{ URL::asset('assets/app/menu/manage.js') }}"></script>
@stop