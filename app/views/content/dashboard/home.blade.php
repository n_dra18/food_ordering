@extends ('base.layout')

@section ('content')
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Dashboard
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- Info boxes -->
  <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box" id="popoverdata" data-content="1 Nasi Goreng | 2 Milkshake | 5 Kentang Goreng" rel="popover" data-placement="bottom" data-original-title="Detail" data-trigger="hover">
        <span class="info-box-icon bg-aqua"><i class="glyphicon glyphicon-cutlery"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Current Cooking</span>
          <span class="info-box-number">Tabel 1</span>
          <span class="info-box-text">Candra</span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-th-list"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">New Order</span>
          <span class="info-box-number">Tabel 2</span>
          <span class="info-box-text">Sam Fajar Muharram</span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Sales</span>
          <span class="info-box-number">760<small> Menus</small></span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
        <div class="info-box-content">
          <span class="info-box-text">Income Today</span>
          <span class="info-box-number">Rp2,000</span>
        </div><!-- /.info-box-content -->
      </div><!-- /.info-box -->
    </div><!-- /.col -->
  </div><!-- /.row -->

  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Monthly Recap Report</h3>
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <div class="btn-group">
              <button class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i></button>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>
            <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-8">
              <p class="text-center">
                <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong>
              </p>
              <div>
                <!-- Sales Chart Canvas -->
                <canvas id="salesChart" style="height: 180px;"></canvas>
              </div><!-- /.chart-responsive -->
            </div><!-- /.col -->
            <div class="col-md-4">
              <p class="text-center">
                <strong>Goal Completion</strong>
              </p>
              <div class="progress-group">
                <span class="progress-text">Menu Sold</span>
                <span class="progress-number"><b>160</b>/3000</span>
                <div class="progress sm">
                  <div class="progress-bar progress-bar-aqua" style="width: 20%"></div>
                </div>
              </div><!-- /.progress-group -->
              <div class="progress-group">
                <span class="progress-text">Income</span>
                <span class="progress-number"><b>Rp10.000.000</b>/Rp45.000.000</span>
                <div class="progress sm">
                  <div class="progress-bar progress-bar-red" style="width: 80%"></div>
                </div>
              </div><!-- /.progress-group -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- ./box-body -->
        <div class="box-footer">
          <div class="row">
            <div class="col-sm-6">
              <div class="description-block border-right">
                <span class="description-percentage text-green"><i class="fa fa-caret-up"></i> 20%</span>
                <h5 class="description-header">Rp10.000.000</h5>
                <span class="description-text">TOTAL PROFIT</span>
              </div><!-- /.description-block -->
            </div><!-- /.col -->
            <div class="col-sm-6">
              <div class="description-block">
                <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 18%</span>
                <h5 class="description-header">1200</h5>
                <span class="description-text">GOAL COMPLETIONS</span>
              </div><!-- /.description-block -->
            </div>
          </div><!-- /.row -->
        </div><!-- /.box-footer -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
@stop