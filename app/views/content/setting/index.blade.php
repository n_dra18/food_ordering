@extends ('layout.layout')

@section ('content')
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		<h3><i class="fa fa-angle-right"></i> Settings</h3>
		<!-- <form action="" method="POST"> -->
		<!-- BASIC FORM ELELEMNTS -->
		<form class="form-horizontal style-form" method="POST" enctype="multipart/form-data">
		<input style="margin-left: 10px;" type="submit" name="submit" value="Save Settings" data-toggle="modal" id="add" href="#" class="btn btn-theme02">
		<div class="row mt">
			<div class="col-lg-12">
				<div class="form-panel">
					<h4 class="mb"><i class="fa fa-angle-right"></i> Your Data Restaurant</h4>
						@foreach($settings as $setting)
						@if($setting->key == 'resto_name')
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Resto Name</label>
							<div class="col-sm-10">
								<input type="text" name="{{ $setting->key }}" value="{{ $setting->values }}" class="form-control">
							</div>
						</div>
						@endif
						@if($setting->key == 'resto_address')
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Resto Address</label>
							<div class="col-sm-10">
								<input type="text" name="{{ $setting->key }}" value="{{ $setting->values }}" class="form-control">
							</div>
						</div>
						@endif
						@if($setting->key == 'resto_tlp')
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Telp</label>
							<div class="col-sm-10">
								<input type="text" name="{{ $setting->key }}" value="{{ $setting->values }}" class="form-control">
							</div>
						</div>
						@endif
						@if($setting->key == 'resto_logo_path')
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label" for="exampleInputFile">Resto Logo</label>
							<div class="col-sm-10">
							<input name="{{ $setting->key }}" type="file">
								<p class="help-block">Format image must JPG or PNG</p>
								@if($setting->values == 'Not Set')
								<img id="img" src="{{ URL::asset('assets/img/no_photo.jpg') }}" width="120">
								@else
								<img id="img" src="{{ URL::asset('uploads/Settings') }}/{{ $setting->values }}" width="120">
								@endif
							</div>          
						</div>
						@endif
						@endforeach                                                                             
				</div>
			</div><!-- col-lg-12-->      	
		</div><!-- /row -->

		<div class="row mt">
			<div class="col-lg-12">
				<div class="form-panel">
					<h4 class="mb"><i class="fa fa-angle-right"></i> Your Data Printer</h4>
						@foreach($settings as $setting)
						@if($setting->key == 'dest_print_chasier')
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Printer Chasier</label>
							<div class="col-sm-10">
								<input type="text" name="{{ $setting->key }}" value="{{ $setting->values }}" class="form-control">
								<span class="help-block">This location printer of chasier when chasier will be printing receipt transaction.</span>
								<span class="help-block">Example : //192.168.0.1/mp280</span>
							</div>
						</div>
						@endif
						@if($setting->key == 'dest_print_report')
						<div class="form-group">
							<label class="col-sm-2 col-sm-2 control-label">Printer Report</label>
							<div class="col-sm-10">
								<input type="text" name="{{ $setting->key }}" value="{{ $setting->values }}" class="form-control">
								<span class="help-block">This location printer for print the report.</span>
								<span class="help-block">Example : //192.168.0.1/mp280</span>
							</div>
						</div>
						@endif
						@endforeach                                                                               
				</div>
			</div><!-- col-lg-12-->      	
		</div><!-- /row -->
		</form>         	
		<!-- </form> -->
	</section><! --/wrapper -->
</section><!-- /MAIN CONTENT -->
@stop