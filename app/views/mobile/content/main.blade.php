@extends ('mobile.layout.layout')

@section ('content')
<div class="se-pre-con"></div>
<div id="first-form"> 
	<h1 style="text-align:center;">Welcome Sam !</h1>
	<p>Any order ? Please input the table number before.</p>
	<fieldset class="ui-grid-a">
		<label>Table Number</label>
		<select name="table_menu" id="table" data-mini="true">
		<option value="">.: Table Number :.</option>
		</select>
		<input name="behalf_of" type="text" placeholder="On Behalf of" />
		<button data-transition="pop" type="submit" id="submit-first" data-theme="a">Go Order !</button>
	</fieldset>
</div>

<div id="second-form">
	<fieldset class="ui-grid-a">
		<h1 style="text-align:center; margin:0;">Search Menu !</h1>
		<input id="autocomplete_menus" data-type="search" placeholder="Search Menus">
	</fieldset>
</div>

<ul id="current_menu_order" data-role="listview" data-inset="true">
</ul>
<button type="submit" id="submit_order"  data-theme="b">Submit Order</button>

<div data-role="popup" id="popupDialog">
	<p>Failed Make Order Data, Please Try Again !<p>
</div>
@stop