    <div data-role="panel" id="panel" data-position="left" data-theme="a" data-display="overlay">
      <h1 style="margin-top: -10px;">Navigations</h1>
      <ul data-role="listview">
      <li><a data-ajax='false' id="home" href="{{URL::to('/')}}">Home</a></li>
      	<li><a data-ajax='true' id="rec_menu" href="#bar">Recomended Menus</a></li>
        <li><a data-ajax='false' id="logout" href="{{URL::to('/logout')}}">Logout</a></li>
      </ul>      
    </div>