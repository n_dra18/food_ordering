<!DOCTYPE html>
<html>
<head>
  @include('mobile.layout.head')
</head>
<script type="text/javascript">
  $(window).load(function() {
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");;
  })
</script>
<style type="text/css">
  .ui-autocomplete{
    list-style: none;
    background-color: #2c3e50;
    padding-left: 10px;
    width: 300px;
    border-radius: 5px;
  }
  .ui-autocomplete li{
    padding: 10px;
    color: white;
  }

  .no-js #loader { display: none;  }
  .js #loader { display: block; position: absolute; left: 100px; top: 0; }
  .se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(Preloader_3.gif) center no-repeat #fff;
  }   
</style>
<script src="{{ URL::asset('/assets/js/socket.io.js') }}"></script>
<script>
$(document).ready(function(){

  var socket = io.connect('192.168.0.104:8080');
  
  /**
  * declare var
  */
  var table_number;
  var behalf_of;
  var current_menu = [];
  var writer = '{{$user_id}}';

  var first_form = $("#first-form");
  var second_form = $("#second-form");

  /**
  * Function show Loading
  */
  function showLoading (param) {
    $.mobile.loading( param, {
      text: 'Please Wait...',
      textVisible: true,
      theme: 'b',
      html: ""
    });     
  } 

  $("#second-form").hide();

  /**
  * Handle the first for show page
  */
  $("#submit-first").click(function(){
    table_number = first_form.find('select[name="table_menu"]').val();
    behalf_of = first_form.find('input[name="behalf_of"]').val();
    $("#tabel_number").val(table_number);
    $("#behalf_of").val(behalf_of);
    $("#id_writer").val();      
    first_form.hide();
    second_form.show();
  });

  /**
  * Auto Complite teh menus
  */
  $( "#autocomplete_menus" ).autocomplete({
    source: function( request, response ) {
      $.ajax({
        url: "{{URL::to('/ajax/getallmenus')}}",
        data : { search : request.term, current_menu : current_menu},
        method: "POST",         
        success: function( data ) {
          response(jQuery.parseJSON(data));
        }
      });
    },
    select: function(event, ui){
      $("#current_menu_order")
      .append('<li id="menus-'+ui.item.id_menus+'" style="margin-bottom:10px" class=" list-menu ui-li-static ui-body-inherit ui-first-child ui-last-child">'+
      ui.item.label+'<div class="ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset">'+
      '<input type="hidden" name="nama_menu" value="'+ui.item.label+'" class="qty"/></div>'+
      '<input type="number" name="qty" class="qty"/></div>'+
      '<button type="button" data-id_menus="'+ui.item.id_menus+'" data-menu="menus-'+
      ui.item.id_menus+'" class="cancel ui-btn ui-shadow ui-corner-all" style="width: 100px;padding: 0px;">Cancel</button></li>');
      $(this).attr("value","");
      $("#qty-"+ui.item.id_menus+":first").focus();
      current_menu.push(ui.item.id_menus);
      return false;
    } 
  });
  
  /**
  * For The show hide loading ajax
  */
  $(document).on({
    ajaxStart : function(){showLoading('show');},
    ajaxStop  : function(){showLoading('hide');}
  });   

  /**
  * For teh handle cancel menu
  */
  $("#current_menu_order").on("click", ".cancel", function(){
    var data_menu = $(this).attr("data-menu");
    var id_menus = parseInt($(this).attr("data-id_menus"));
    var index = current_menu.indexOf(id_menus);
    current_menu.splice(index,1)
    $("#"+data_menu).remove();
  });

  /**
  * Set Available Table Number
  */
  $.ajax({
    url: "{{URL::to('/ajax/availableTable')}}",
    method: "GET",          
    success: function( data ) {
      var data = jQuery.parseJSON(data);
      $.each(data, function(k, v){
        $("#table").append('<option value="'+v+'">'+v+'</option>');
      });
    }
  });

  $('#logout').click(function(){
    showLoading('show');
  });

  $('#submit_order').click(function(){
    menus = [];
    $('.list-menu').each(function(){
      var menu = {
        id  : $(this).find('button').attr('data-id_menus'),
        qty : $(this).find('input[name="qty"]').val(),
        nama_menu : $(this).find('input[name="nama_menu"]').val()
      };
      menus.push(menu)
    })

    var dataOrder = {
      behalf_of : behalf_of,
      table_number : table_number,
      id_writer : writer,
      menus : menus
    }

    pushOrderToServer(dataOrder);
  });


  function pushOrderToServer(dataOrder) {

      $.ajax({
        url: "{{URL::to('/order')}}",
        data : {dataOrder : dataOrder},
        type: "POST",         
        success: function( data ) {
          if (data.success == 1) {
            socket.emit('push notif', data);
            window.location.href = "http://192.168.0.104/food_ordering/public/order";
          } else {
            $('#popupDialog').popup('open');
          };
        }
      });
  }
}); 




</script>
<body>
  <div data-role="page" id="foo">

    @include('mobile.content.panel.index')
    @include('mobile.layout.header')

    <div data-role="content" role="main">
      @yield('content')
    </div>
  </div>

  <!-- Start of recomended menu page -->
  <div data-role="page" id="bar">
    <div data-role="header">
      <h1>Recomended Menu</h1>
      <a href="#foo" class="ui-btn-right ui-btn ui-btn-b ui-btn-inline ui-mini ui-corner-all ui-btn-icon-right">Back</a>
    </div>

    <div role="main" class="ui-content">
      <h3>This data recomended menu in this week</h3>
      <ul data-role="listview" data-inset="true">
      @foreach($numberOfOrders as $numberOfOrder)
        <li><a href="#">
          <img style="margin-top: 10px;margin-left: 5px;border-radius:none;border-radius: 5px;" src="{{ URL::asset('uploads/menus')}}/{{ $numberOfOrder->base_foto }}">
          <h2>{{ $numberOfOrder->nama_menus }}</h2>
          <input type="hidden" name="nama_menu">
          <p>Telah dipesan sebanyak {{ $numberOfOrder->dipesan }}</p></a>
        </li>
      @endforeach
      </ul>
    </div><!-- /content -->
  </div><!-- /page -->


</body>
</html>
