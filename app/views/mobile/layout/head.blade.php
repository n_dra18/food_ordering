  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{ URL::asset('assets/css/jquery.mobile.flatui.css') }}" />
  <script src="{{ URL::asset('assets/js/jquery-1.8.3.min.js') }}"></script>
  <script src="{{ URL::asset('assets/js/jquery-ui.js') }}"></script>
  <script src="{{ URL::asset('assets/js/jquery.mobile-1.4.0-rc.1.js') }}"></script>