<?php

/**
*
* @author Sam Fajar <samfajar1206@gmail.com>
* @license UNIKOM (Universitas Komputer Indonesia)
* @package Food Ordering App 
*
*/

namespace FoodOrdering;

use View;
use Route;
use Redirect;
use Lang;
use Request;
use Response;

class MenusController extends BaseController implements CRUDController
{

	public function manage()
	{
		// get all menus
		$menus = Menus::where('is_deleted', '=', 0)->get();

		//  return response view manage menus
		return View::make('content.menus.manage-menus')
		->with('menus', $menus)
		;		

	}

	public function getAdd()
	{
		return View::make('content.menus.add-menus');
	}

	public function postAdd()
	{
		$form_processor = new MenuAddFormProcessor();

		if ($form_processor->validate() === FALSE) {
			$errors = $form_processor->getErrors();

			return Redirect::back()
			->withErrors($errors);
		}

		$data = $form_processor->getData();
		$processor = new MenuAddDataProcessor();

		if ($processor->process($data) === FALSE) {
			
			$errors = $processor->getError();
			return Redirect::back()
			->withErrors($errors);

		} else {

			if ($data['action'] == 'add_more') {
				return Redirect::back()
				->withSuccess(Lang::get('general.save_success'));
			} else {
				return Redirect::route('menus_manage')
				->withSuccess(Lang::get('general.save_success'));				
			}

		}
	
	}

	public function getEdit()
	{
		// get by segment 3
		$id = Request::segment(3);
		$menu = Menus::find($id);
		
		return View::make('content.menus.edit-menus')
		->with(array(
			'menu' => $menu,
			));
	}

	public function postEdit()
	{
		# code...
	}

	public function delete()
	{
		try {
			// get by segment 3
			$id = Request::segment(3);

			// find menu by id
			$menu = Menus::find($id);

			// menu delete
			$menu->deleteMenus();

			// return redirect
			return Redirect::route('menus_manage')
			->with('message', array(
				'status' => 'success',
				'message' => Lang::get('general.delete_success'))
			);

		} catch (Exception $e) {
			
			// return redirect
			return Redirect::route('menus_manage')
			->with('message', array(
				'status' => 'failed',
				'message' => Lang::get('general.delete_failed'))
			);
			
		}
	}

	public function enable()
	{
		$id = Request::segment(3);
		
		$processor = new MenuEnableDataProcessor();
		
		if($processor->process($id)){
			return Response::json(array(
				'status' => 'success', 
				'message' => 'success',
				));
		} else {
			return Response::json(array(
				'status' => 'failed',
				'message' => $processor->getError(),
				));
		}
	}

	public function disable()
	{
		$id = Request::segment(3);
		
		$processor = new MenuDisableDataProcessor();
		
		if($processor->process($id)){
			return Response::json(array(
				'status' => 'success', 
				'message' => 'success',
				));
		} else {
			return Response::json(array(
				'status' => 'failed',
				'message' => $processor->getError(),
				));
		}
	}	


	public function processEdit()
	{
		try {
			// get by segment 3
			$id = Request::segment(3);

			// get all input menus
			$input = Input::all();	

			// find menus by id
			$menus = Menus::find($id);

			$menus->updateMenus($input);

			// return redirect
			return Redirect::route('menus_manage')
			->with('message', array(
				'status' => 'success',
				'message' => Lang::get('general.save_success'))
			);

		} catch (Exception $e) {
			// return redirect
			return Redirect::route('menus_manage')
			->with('message', array(
				'status' => 'failed',
				'message' => Lang::get('general.save_failed'))
			);
		}
	}
}