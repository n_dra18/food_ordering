<?php

/**
*
* @author Sam Fajar <samfajar1206@gmail.com>
* @license UNIKOM (Universitas Komputer Indonesia)
* @package Food Ordering App 
*
*/

class TransactionController extends BaseController
{

	public function manage()
	{
		$orders = Order::with('table', 'detailPesanan')
		->where('created_at', '>=', date('Y-m-d'))
		->where('status', '!=', 2)
		->get();

		$notif = Notification::getNotif();

		return View::make('content.transaction.manage-transaction')
		->with('orders', $orders)
		->with('notif', $notif)
		;

	}

	public function finished()
	{
		if (!Entrust::hasRole('Cashier'))
			return Response::view('content.errors.notFound', array(), 403);

		try {
			// get id order
			$order_id = Input::get('id_transaction');

			// initial object model order
			$order = Order::find($order_id);
			
			$transaction = new Transaction;
			$transaction->createTransaction($order);

			return Redirect::route('transaction');

		} catch (Exception $e) {
			echo "ERROR";
		}
		

	}
}