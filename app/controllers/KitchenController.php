<?php

class KitchenController extends BaseController {

	public function getKitchen()
	{
		$orders = Order::with('table', 'detailPesanan')
		->where('status', '=', 1)
		->where('created_at', '>=', date('Y-m-d'))
		->get();
		
		return View::make('kitchen.content.main')
		->with('orders', $orders);
		;
	}

	public function apply()
	{
		//get by segment 3
		$id = Request::segment(3);

		//order find id
		$order = Order::find($id);

		//order update status
		$order->status=0;

		//order save to database
		$order->save();

		//return to method getKitchen
		return Redirect::route('kitchen');

	}
}
