<?php

/**
*
* @author Sam Fajar <samfajar1206@gmail.com>
* @license UNIKOM (Universitas Komputer Indonesia)
* @package Food Ordering App 
*
*/
namespace FoodOrdering;

use Redirect;
use Auth;

class UserController extends BaseController
{
	
	public function manage()
	{		
		// get all user
		$users = User::where('is_deleted', '=', 0)->get();

		$notif = Notification::getNotif();
		// echo "<pre>";
		// print_r($notif);
		// die();
		//  return response view manage user
		return View::make('content.users.manage-users')
		->with('users', $users)
		->with('notif', $notif)
		->with('messageOtherFunction', Session::get('message'))
		;
	}

	public function getAdd()
	{
		// get all input user
		$input = Input::all();

		//create user model as object
		$user = new User;

		//create user
		$user->createUser($input);

		// return redirect
		return Redirect::route('user_manage');
	}

	public function postAdd()
	{
		// get all input user
		$input = Input::all();

		//create user model as object
		$user = new User;

		//create user
		$user->createUser($input);

		// return redirect
		return Redirect::route('user_manage');
	}	

	public function delete()
	{	
		try {
			// get by segment 3
			$id = Request::segment(3);

			// find user by id
			$user = User::find($id);

			// user delete
			$user->deleteUser();

			// return redirect
			return Redirect::route('user_manage')
			->with('message', array(
				'status' => 'success',
				'message' => Lang::get('general.delete_success'))
			);

		} catch (Exception $e) {
			
			// return redirect
			return Redirect::route('user_manage')
			->with('message', array(
				'status' => 'failed',
				'message' => Lang::get('general.delete_failed'))
			);
			
		}

	}

	public function getEdit()
	{
		try {
			// get by segment 3
			$id = Request::segment(3);

			// find user by id
			$user = User::find($id);
			// $user->password = Crypt::decrypt($user->password);

			echo json_encode($user);

		} catch (Exception $e) {
			
			
		}		
	}

	public function postEdit()
	{
		try {
			// get by segment 3
			$id = Request::segment(3);

			// get all input user
			$input = Input::all();	

			// find user by id
			$user = User::find($id);

			$user->updateUser($input);

			// return redirect
			return Redirect::route('user_manage')
			->with('message', array(
				'status' => 'success',
				'message' => Lang::get('general.save_success'))
			);

		} catch (Exception $e) {
			// return redirect
			return Redirect::route('user_manage')
			->with('message', array(
				'status' => 'failed',
				'message' => Lang::get('general.save_failed'))
			);
		}		
	}

	public function login()
	{

		$form_processor = new UserLoginFormProcessor();

        if($form_processor->validate() === FALSE)
        {
            $errors = $form_processor->getErrors();
            return Redirect::back()->withErrors($errors);
        }

        $data = $form_processor->getData();
		$processor = new UserLoginDataProcessor();

		if ($processor->process($data) === TRUE) {
				return Redirect::route('/');

		} else {

            $errors = $processor->getError();
            return Redirect::back()->withErrors($errors);
		
		}
	}

	public function logout()
	{
		Auth::logout();

		return Redirect::route('/');
	}
}