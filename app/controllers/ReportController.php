<?php

/**
*
* @author Sam Fajar <samfajar1206@gmail.com>
* @license UNIKOM (Universitas Komputer Indonesia)
* @package Food Ordering App 
*
*/

class ReportController extends BaseController
{
	public function getReport(){
		$notif = Notification::getNotif();
		$report = array();
		if (Input::get('start-date') && Input::get('end-date')){
			$report = Report::viewReport(Input::all());
		}
		return View::make('content.report')
		->with('notif', $notif)
		->with('report', $report)
		;
	}
}
