<?php

/**
* This the controller all of order 
*
* @author Sam Fajar <samfajar1206@gmail.com>
* @license UNIKOM (Universitas Komputer Indonesia)
* @package Food Ordering App 
*
*/

class OrderController extends BaseController
{

	/**
	* This Constructor Method
	*/
	public function __construct() {

	}

	/**
	* this function to handle the writers do an order
	* @return View Order (List Menu)
	*/
	public function getOrder()
	{	

		$numberOfOrder = DB::table('menus')
        ->join('detail_pesanan', 'detail_pesanan.id_menu', '=', 'menus.id_menus')
		->select(
			DB::raw('menus.nama_menus nama_menus, 
				menus.base_photo base_foto, 
				menus.stock as stock, 
				(select sum(jumlah) from detail_pesanan where detail_pesanan.id_menu = menus.id_menus) as dipesan'
			)
		)
		->where('detail_pesanan.created_at', '>=', date('Y-m-d 00:00:00', strtotime("-7 days")))
	    ->orderBy('jumlah', 'desc')
	    ->groupBy('menus.id_menus')		
		->get();

		return View::make('mobile.content.main')
		->with('user_id', Auth::id())
		->with('numberOfOrders', $numberOfOrder)
		;
	}

	/**
	* This function to handle the proccess of an order
	* @return View Dashboar Writers
	*/
	public function proccessOrder()
	{

		DB::beginTransaction();
		$dataOrder = Input::get('dataOrder');

		// try {

			$order = new Order();
			$order->makeOrder($dataOrder);
		
			DB::commit();

			$dataOrder['success'] = 1;
			return Response::json($dataOrder);

		// } catch (Exception $e) {
		// 	DB::rollback();

		// 	$dataOrder['success'] = 0;
		// 	return Response::json($dataOrder);
		// }
	}
}