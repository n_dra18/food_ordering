<?php

/**
*
* @author Sam Fajar <samfajar1206@gmail.com>
* @license UNIKOM (Universitas Komputer Indonesia)
* @package Food Ordering App 
*
*/

use Order\Pending as OrderPending;

class AjaxController extends BaseController
{
	public function __construct() {
		if (!Request::ajax()) {
			die('ERROR');
		}
	}

	public function getAllMenus()
	{
		$list_menus = [];

		$name = Input::get('search');
		$current_menus = Input::get('current_menu');


		$menus = Menus::select('nama_menus', 'id_menus', "stock")
		->where('nama_menus', 'LIKE', '%'.$name.'%')
		->where('is_deleted', '=', 0);
		if (!empty($current_menus)) {
			$menus = $menus
			->whereNotIn('id_menus', $current_menus);
		}
		$menus = $menus
		->get();

		foreach ($menus as $menu) {
			array_push($list_menus, array(
				"label" => $menu->nama_menus,
				"id_menus" => $menu->id_menus,
				"stock" => $menu->stock
			));
		}

		echo json_encode($list_menus);
	}

	public function availableTable()
	{

		$availableTables = Table::where('is_available', '=', 1)->get();
		$tables = [];

		if (!empty($availableTables)) {
			foreach ($availableTables as $table) {
				array_push($tables, $table->id_meja);
			}
		}

		return json_encode($tables);
	}

	public function getDetailTransaction()
	{
		$id = Input::get('id_pemesanan');

		//order find id
		$order = Order::find($id);

		foreach ($order->detailPesanan as $key => $detail) {
			
			$p = new StdClass;
			$p = (object)$detail;
			$p->menu = $detail->menu;	
			
			$orderDetails[] = $p;
		}

		$price = new Transaction;
		$price = $price->calcullatePrice($order);

		$transaction = new StdClass;
		$transaction->order = $order;
		$transaction->price = $price;

		echo json_encode($transaction);
		die();
	}
}