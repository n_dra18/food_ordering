<?php

/**
*
* @author Sam Fajar <samfajar1206@gmail.com>
* @license UNIKOM (Universitas Komputer Indonesia)
* @package Food Ordering App 
*
*/

class SettingController extends BaseController
{
	public function index(){

		if (Input::get('submit')) {
			Setting::saveSetings(Input::all());
		}
		
		$settings = Setting::all();

		return View::make('content.setting.index')
		->with('settings', $settings);
	}

}
