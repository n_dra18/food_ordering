<?php

namespace FoodOrdering;

/**
* 
*/

interface CRUDController
{
	public function manage();

	public function getAdd();

	public function postAdd();

	public function getEdit();

	public function postEdit();

}