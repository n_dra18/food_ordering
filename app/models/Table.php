<?php

class Table extends Eloquent 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'meja';

	/**
	 * Set primary key name.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id_meja';		

	/**
	* Relation to Pesanan
	*/	
	public function table()
	{
		return $this->hasMany('Order', 'id_meja');
	}

	/*
	*
	*/
	public function changeStatusAvailable($status)
	{
		$this->is_available = $status;
		$this->save();

		return true;
	}	
}