<?php

class Report extends Eloquent 
{
	public static function viewReport($input){

		$input['start-date'] = date('Y-m-d 00:00:00', strtotime($input['start-date']));
		$input['end-date'] = date('Y-m-d 00:00:00', strtotime($input['end-date'].'+1 days'));
		
		$report = Transaction::with('pesanan')
		->whereBetween('created_at', array($input['start-date'], $input['end-date']))
		->get();
		// echo "<pre>";
		// print_r($report);
		// die();
		return $report;
	}
}

