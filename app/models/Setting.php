<?php

class Setting extends Eloquent 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'settings';

	public static function saveSetings($input)
	{
		foreach ($input as $key => $value) {

			// add update foto
			if ($key == 'resto_logo_path') {

				$beforeFile = DB::table('settings')
				->where('key', '=', 'resto_logo_path')
				->first();

				if ($input['resto_logo_path'] != NULL) {

					if ($beforeFile->values != 'Not Set') {
						unlink('uploads/Settings/'.$beforeFile->values);
					}

					$extension = $input[$key]->getClientOriginalExtension();			
					$value = 'logo_resto'.'.'.$extension;
					$destinationPath = 'uploads/Settings';
					$input['resto_logo_path']->move($destinationPath, $value);	
				
				} else {
					$value = $beforeFile->values;
				}
			}

			DB::table('settings')
			->where('key','=', $key)
			->update(['values' => $value]);				
		}
	}
}