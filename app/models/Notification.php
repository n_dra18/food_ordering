<?php

class Notification extends Eloquent
{

	public static function getNotif(){
		$transaction = new Order();
		$notif = $transaction->orderBy('created_at','desc')
		->take(3)
		->where('created_at', '>=', date('Y-m-d'))
		->get();

		return $notif;
	}


}