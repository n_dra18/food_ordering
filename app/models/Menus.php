<?php

namespace FoodOrdering;

use Eloquent;
use Image;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Menus extends Eloquent implements SluggableInterface
{
	use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'nama_menus',
        'save_to'    => 'slug',
    ];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'menus';

	/**
	 * Set primary key name.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id_menus';	

	/**
	* Relation to Detail Pesanan
	*/	
	public function detailPesanan()
	{
		return $this->hasMany('Detail', 'id_menu');
	}

	public function createMenus($data)
	{
		// Insert to database
		$this->nama_menus = $data['menuname'];
		$this->harga = $data['price'];
		$this->stock = $data['stock'];
		$this->base_photo = $data['menuname'].'.jpg';
		$this->can_order = $data['can_order'] ? 1 : 0;
		$this->keterangan = $data['keterangan'];
		$this->save();	

		return $this;
	}

	public function deleteMenus()
	{
		if (!empty($this->base_photo)) {
			unlink(public_path().DIRECTORY_SEPARATOR.'uploads/menus'.DIRECTORY_SEPARATOR.$this->base_photo);
		}
		
		$this->is_deleted = 1;
		$this->save();

		return true;
	}

	public function updateMenus($menu)
	{
		$filename = NULL;
		if (!empty($menu['photo'])) {
			$extension = $menu['photo']->getClientOriginalExtension();
			$filename = $menu['nama_menus'].'.'.$extension;
		}

		// Insert to database
		$this->nama_menus = $menu['nama_menus'];
		$this->harga = $menu['price'];
		$this->stock = $menu['stock'];
		
		if (!empty($menu['photo'])) {
			$this->base_photo = $filename;
		}

		$this->save();

		if ($this->id_menus > 0) {

			if (!empty($menu['photo'])) {
				$destinationPath = 'uploads/menus';
				$menu['photo']->move($destinationPath, $filename);
			}
		}			

		return $this;		
	}

	public function decraseStock($qty)
	{
		$this->stock -= $qty;
		$this->save();
	}

	public function enable()
	{
		$this->can_order = 1;
		$this->save();

		return $this;
	}

	public function disable()
	{
		$this->can_order = 0;
		$this->save();

		return $this;
	}		

}