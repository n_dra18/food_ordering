<?php

use Order\Detail as OrderDetail;
class Order extends Eloquent 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'pesanan';

	/**
	 * Set primary key name.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id_pemesanan';		

	/**
	* Relation to User
	*/
	public function user()
	{
		return $this->belongsTo('User', 'id_user');
	}

	/**
	* Relation to Table
	*/	
	public function table()
	{
		return $this->belongsTo('Table', 'id_meja');
	}

	/**
	* Relation to Detail Pesanan
	*/	
	public function detailPesanan()
	{
		return $this->hasMany('Detail', 'id_pemesanan');
	}

	/**
	* Relation to Pesanan
	*/	
	public function transaction()
	{
		return $this->hasOne('Transaction', 'id_pesanan');
	}


	public function makeOrder($dataOrder)
	{
		$this->id_user = $dataOrder['id_writer'];
		$this->id_meja = $dataOrder['table_number'];
		$this->nama_pemesan = $dataOrder['behalf_of'];
		$this->save();

		foreach ($dataOrder['menus'] as $menu) {

			$orderDetail = new OrderDetail();
			$orderDetail->createDetailPesanan($menu['id'], $this, $menu['qty']);
			
		}

		$table = Table::find($dataOrder['table_number']);
		$table->changeStatusAvailable(0);
		
	}			
}