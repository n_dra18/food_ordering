<?php

class Detail extends Eloquent 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'detail_pesanan';


	/**
	* Relation to Pesanan
	*/	
	public function pesanan()
	{
		return $this->belongsTo('Order', 'id_pemesanan');
	}	

	/**
	* Relation to Menus
	*/	
	public function menu()
	{
		return $this->belongsTo('Menus', 'id_menu');
	}
		
}