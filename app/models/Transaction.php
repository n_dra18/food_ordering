<?php

class Transaction extends Eloquent  
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'transaksi';

	/**
	* Relation to Pesanan
	*/	
	public function pesanan()
	{
		return $this->belongsTo('Order', 'id_pesanan');
	}

	public function calcullatePrice($order)
	{
		$subtotal = $this->getSubtotal($order->detailPesanan);
		$taxandservice = $this->getTaxAndService($subtotal);
		$total = $this->getTotal($subtotal, $taxandservice);


		$price = new StdClass;
		$price->subtotal = $subtotal;
		$price->taxandservice = $taxandservice;
		$price->total = $total;

		return $price;
	}

	public function getSubtotal($details)
	{
		$subtotal = 0;

		foreach ($details as $value) {
			$subtotal+=$value->subtotal_pemesanan;
		}

		return $subtotal;
	}

	public function getTaxAndService($subtotal)
	{
		return 0.12 * $subtotal;
	}

	public function getTotal($subtotal, $taxandservice)
	{
		return $subtotal + $taxandservice;
	}

	/**
	* This method for create transaction by Chasier
	* @param Object ORM $order is order
	* @return boolean is created or not
	*/
	public function createTransaction($order)
	{
		// calculated price of order
		$price = $this->calcullatePrice($order);

		// set value to database
		$this->id_pesanan = $order->id_pemesanan;
		$this->tgl_transaksi = date('Y-m-d');
		$this->subtotal_transaksi = $price->subtotal;
		$this->total = $price->total;
		// saves
		if($this->save()){
			// update order to has paid
			$order->status  = 2;
			$order->save();

			// update status table
			$meja = Table::find($order->id_meja);
			$meja->changeStatusAvailable(1);

			// print struk
			$this->printStruk($this, $order);
		} else {
			return false;
		}
	}

	public function printStruk($transaction, $order)
	{
		$resto_name = Setting::where('key', '=', 'resto_name')->first();
		$resto_addr = Setting::where('key', '=', 'resto_address')->first();
		$resto_tlp = Setting::where('key', '=', 'resto_tlp')->first();


		$struk = fopen('struk.txt', 'w') or die('Unable to open file!');

		$fillFtruk = '';
		$fillFtruk = $fillFtruk. $resto_name->values."\n";
		$fillFtruk = $fillFtruk. $resto_addr->values." Telp ".$resto_tlp->values."\n";
		$fillFtruk = $fillFtruk. "=================================================\n";
		$fillFtruk = $fillFtruk. "Nama Menu\tQty\tHarga\tSubtotal\n";
		$fillFtruk = $fillFtruk. "-------------------------------------------------\n";

		foreach ($order->detailPesanan as $item) {
			$fillFtruk = $fillFtruk. $item->menu->nama_menus.
			"\t".$item->jumlah.
			"\t".number_format($item->menu->harga).
			"\t".number_format($item->subtotal_pemesanan).
			"\n";
		}

		$fillFtruk = $fillFtruk."-------------------------------------------------\n";
		$fillFtruk = $fillFtruk."Subtotal\t\t\t\t".number_format($transaction->subtotal_transaksi)."\n";
		$fillFtruk = $fillFtruk."Service and Tax\t\t\t".number_format($transaction->total - $transaction->subtotal_transaksi)."\n";
		$fillFtruk = $fillFtruk."-------------------------------------------------\n";
		$fillFtruk = $fillFtruk."Total\t\t\t\t".number_format($transaction->total)."\n";
		$fillFtruk = $fillFtruk."=================================================\n";
		$fillFtruk = $fillFtruk."       Terima Kasih Atas kunjungan ada\n";

		fwrite($struk, $fillFtruk);
		fclose($struk);
		// exec("NOTEPAD /p struk.txt");
		
	}
}