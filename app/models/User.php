<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Zizaco\Entrust\HasRole;

class User extends Eloquent implements UserInterface, RemindableInterface {
	
	// use EntrustUserTrait;
	use UserTrait, RemindableTrait, HasRole;


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('remember_token');

	/**
	* Relation to Order
	*/
	public function pesanan()
	{
		return $this->hasMany('Order', 'id_user');
	}


	public function createUser($user)
	{
		$filename = NULL;
		if (!empty($user['photo'])) {
			$extension = $user['photo']->getClientOriginalExtension();
			$filename = $user['user_name'].'.'.$extension;
		}

		// Insert to database
		$this->nama_user = $user['name'];
		$this->username = $user['user_name'];
		$this->base_photo = $filename;
		$this->password = Hash::make($user['pass']);
		$this->role = $user['role'];

		$this->save();

		/**
		* Create Photo User
		*/

		if ($this->id > 0) {

			if (!empty($user['photo'])) {
				$destinationPath = 'uploads';
				$user['photo']->move($destinationPath, $filename);
			}
		}

		/**
		* Create the permision
		*/		
		switch ($user['role']) {
			case '1':
					$role = Role::where('name', '=', 'SuperAdmin')->first();
					$this->attachRole($role);
				break;
			case '2':
					$role = Role::where('name', '=', 'Cashier')->first();
					$this->attachRole($role);
				break;
			case '3':
					$role = Role::where('name', '=', 'Writers')->first();
					$this->attachRole($role);
				break;
			case '4':
					$role = Role::where('name', '=', 'Chef')->first();
					$this->attachRole($role);
				break;												
			default:
					$role = Role::where('name', '=', 'SuperAdmin')->first();
					$this->attachRole($role);					
				break;
		}

		return $this;
	}


	public function deleteUser()
	{
		if (!empty($this->base_photo)) {
			unlink(public_path().DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.$this->base_photo);
		}
		
		$this->is_deleted = 1;
		$this->save();

		return true;
	}

	public function updateUser($user)
	{
		$filename = NULL;
		if (!empty($user['photo'])) {
			$extension = $user['photo']->getClientOriginalExtension();
			$filename = $user['user_name'].'.'.$extension;
		}

		// Insert to database
		$this->nama_user = $user['name'];
		$this->username = $user['user_name'];
		
		if (!empty($user['photo'])) {
			$this->base_photo = $filename;
		}

		if (!empty($user['pass'])) {
			$this->password = Hash::make($user['pass']);
		}
		
		$this->role = $user['role'];
		$this->save();

		if ($this->id > 0) {

			if (!empty($user['photo'])) {
				$destinationPath = 'uploads';
				$user['photo']->move($destinationPath, $filename);
			}
		}			

		return $this;		
	}
}
