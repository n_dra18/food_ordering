<?php

namespace  Order;
use Eloquent;

class Pending extends Eloquent 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'order_pending';	

	/**
	 * Set primary key name.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * Set fillable key name.
	 *
	 * @var string
	 */
	protected $fillable = array('id_table', 'id_menus', 'id_writers', 'qty');		

}