<?php

namespace  Order;
use Eloquent;
use Menus;

class Detail extends Eloquent 
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'detail_pesanan';

	/**
	* Relation to Pesanan
	*/	
	public function menu()
	{
		return $this->hasOne('Menus', 'id_menu');
	}		


	/**
	 * Set fillable key name.
	 *
	 * @var string
	 */
	protected $fillable = array('id_pemesanan', 'id_menu', 'jumlah', 'subtotal_pemesanan');	

	public function createDetailPesanan($idMenus, $order, $qty)
	{
		$menu = Menus::find($idMenus);

		$this->id_pemesanan = $order->id_pemesanan;
		$this->id_menu = $menu->id_menus;
		$this->jumlah = $qty;
		$this->subtotal_pemesanan = $menu->harga * $qty;
		$this->save();

		$menu->decraseStock($qty);

		return $this;
	}

	public function withMenu()
	{
		return $this->with('menu')->get();
	}	

}