<?php

	namespace FoodOrdering;
	use Input;

	class UserLoginFormProcessor extends BaseFormProcessor
	{
		private $username;
		private $password;
		
		public function getInput()
		{
			$this->username = Input::get('username');
			$this->password = Input::get('password');
		}

		public function setValidationData()
		{
			$this->data = array(
	            'username' => $this->username,
	            'password' => $this->password,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
	            'username' => 'required',
	            'password' => 'required',
			);
		}
	}