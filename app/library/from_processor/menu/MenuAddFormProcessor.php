<?php

namespace FoodOrdering;

use Input;
/**
* 
*/
class MenuAddFormProcessor extends BaseFormProcessor
{
		private $menuname;
		private $price;
		private $stock;
		private $image;
		private $action;
		private $can_order;
		private $keterangan;
		
		public function getInput()
		{
			$this->menuname = Input::get('menuname');
			$this->price = Input::get('price');
			$this->stock = Input::get('stock');
			$this->image = Input::file('image');
			$this->can_order = Input::get('can_order');
			$this->keterangan = Input::get('keterangan');

			if (Input::get('add')) {
				$this->action = Input::get('add');
			} else {
				$this->action = Input::get('add_more');
			}
		}

		public function setValidationData()
		{
			$this->data = array(
				'menuname' => $this->menuname,
				'price' => $this->price,
				'stock' => $this->stock,
				'image' => $this->image,
				'action' => $this->action,
				'can_order' => $this->can_order,
				'keterangan' => $this->keterangan,
			);
		}

		public function setValidationRules()
		{
			$this->rules = array(
				'menuname' => 'required|unique:menus,nama_menus',
				'price' => 'required',
				'stock' => 'required|numeric',
				'image' => 'image|max:1000',
			);
		}	
}