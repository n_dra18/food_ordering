<?php
	
	namespace FoodOrdering;
	
	class MenuAddDataProcessor extends BaseDataProcessor
	{
		public function process($data)
		{
			try
			{
				$menu = new Menus();
				$menu = $menu->createMenus($data);	
				
				$imageProccessor = new MenuImageDataProcessor($data['image'], $menu->getSlug());
				$imageProccessor->createImage();

				return TRUE;				
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}