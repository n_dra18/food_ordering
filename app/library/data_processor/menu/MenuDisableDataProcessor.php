<?php
	
	namespace FoodOrdering;
	
	class MenuDisableDataProcessor extends BaseDataProcessor
	{
		public function process($id)
		{
			try {

				$menu = Menus::find($id);

				if ($menu->can_order == 1) {
					$menu->disable();
					return TRUE;
				} else {
					$this->error ='Menu allready disable';
					return FALSE;
				}
				
			} catch (\Exception $e) {
				$this->error = $e->getMessage();
				return FALSE;				
			}
		}
	}