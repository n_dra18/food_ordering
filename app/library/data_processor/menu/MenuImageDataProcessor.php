<?php
	
	namespace FoodOrdering;
	use Image;
	
	class MenuImageDataProcessor extends BaseDataProcessor
	{
		private $path = 'public/uploads/menus/';
		private $image;
		private $name;

		public function __construct($image, $name) {
			$this->image = $image;
			$this->name = $name;
		}

		public function createImage()
		{

			$this->createDirectory();
			$this->verySmallImage();
			$this->smallImage();
			$this->mediumImage();
			$this->largeImage();

			return TRUE;
		}
		
		public function verySmallImage()
		{
			Image::make($this->image)
			->resize(50, 40)
			->save($this->path.$this->name.'-verysmall.jpg');
		}

		public function smallImage()
		{
			Image::make($this->image)
			->resize(100, 80)
			->save($this->path.$this->name.'-small.jpg');
		}

		public function mediumImage()
		{
			Image::make($this->image)
			->resize(200, 170)
			->save($this->path.$this->name.'-medium.jpg');
		}

		public function largeImage()
		{
			Image::make($this->image)
			->resize(400, 300)
			->save($this->path.$this->name.'-large.jpg');
		}

		public function createDirectory()
		{
			mkdir($this->path.$this->name);
			$this->path = $this->path.$this->name.'/';
		}
	}