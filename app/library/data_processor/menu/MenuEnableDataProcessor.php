<?php
	
	namespace FoodOrdering;
	
	class MenuEnableDataProcessor extends BaseDataProcessor
	{
		public function process($id)
		{
			try {

				$menu = Menus::find($id);

				if ($menu->can_order == 0) {
					$menu->enable();
					return TRUE;
				} else {
					$this->error ='Menu allready enable';
					return FALSE;
				}
				
			} catch (\Exception $e) {
				$this->error = $e->getMessage();
				return FALSE;				
			}
		}
	}