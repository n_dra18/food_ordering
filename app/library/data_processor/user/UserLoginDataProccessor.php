<?php
	
	namespace FoodOrdering;

	use Auth;
	
	class UserLoginDataProcessor extends BaseDataProcessor
	{
		public function process($data)
		{
			try
			{
				if (!Auth::attempt($data)) {
					$this->error = 'Wrong Username or Password';
					return FALSE;	
				} 

				return TRUE;
			}
			catch(\Exception $e)
			{
				$this->error = $e->getMessage();
				return FALSE;
			}
		}
	}