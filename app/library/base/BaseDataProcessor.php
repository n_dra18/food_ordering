<?php

/**
* 
*/
namespace FoodOrdering;

abstract class BaseDataProcessor
{
	protected $error;

	protected $output;

	public function getError()
	{
		return $this->error;
	}

	public function getoutput()
	{
		return $this->output;
	}	
}