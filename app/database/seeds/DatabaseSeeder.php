<?php

use SeedSettings as SeedSettings;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// $this->call('MejaTableSeeder');
		// $this->command->info('Meja table seeded!');
		// $this->call('UsersTableSeeder');
		// $this->command->info('Users table seeded!');
		// $this->command->info('For the first time you can login as super admin (username  = admin|password = admin)');
		
		// $this->call('MakeRole');
		// $this->command->info('Roles Created');
		// $this->call('CreatePermision');
		// $this->command->info('Permission Created');			
		// $this->call('AddPermissionRole');
		// $this->command->info('Super admin Add Permision manage all success');

		// $this->call('AssignRoleSuperAdmin');
		// $this->command->info('Super has assign role as super admin');

		// seed value setting
		// $this->call('SeedSettings');
		// $this->command->info('Seeder key setting success');
	}

}


class MejaTableSeeder extends Seeder {

	/**
	 * Run the MejaTableSeeder seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$existTable = Table::all();

		if ($existTable->count() <= 0) {
			$dataMeja = [
				['id_meja' => 1, 'quota' => 2, 'created_at' => new DateTime, 'updated_at' => new DateTime ],
				['id_meja' => 2, 'quota' => 3, 'created_at' => new DateTime, 'updated_at' => new DateTime ],
				['id_meja' => 3, 'quota' => 5, 'created_at' => new DateTime, 'updated_at' => new DateTime ],
			];
			DB::table('meja')->insert($dataMeja);			
		} else {
			$this->command->info('Meja already exist, skiped');
		}
	}

}


class UsersTableSeeder extends Seeder {

	/**
	 * Run the UsersTableSeeder seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$existUserAdmin = User::where('username' , '=', 'admin')->first();

		if (empty($existUserAdmin)) {
		$dataUsers = [
				['nama_user' => 'Super Admin', 'username' => 'admin', 'password' => Hash::make('admin'), 'role' => 1, 'created_at' => new DateTime, 'updated_at' => new DateTime ]
			];
			DB::table('users')->insert($dataUsers);
		} else {
			$this->command->info('Users admin already exist, skiped');
		}

	}

}

class MakeRole extends Seeder {

	/**
	 * Run the MakeRoleSuperAdminSeed seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//truncate
		$existTable = Role::all();
		if ($existTable->count() <= 0) {
			$superAdmin = new Role();
			$superAdmin->name = 'SuperAdmin';
			$superAdmin->save();

			$admin = new Role();
			$admin->name         = 'Writers';
			$admin->save();

			$admin = new Role();
			$admin->name         = 'Cashier';
			$admin->save();

			$admin = new Role();
			$admin->name         = 'Chef';
			$admin->save();
		}		
	}

}

class CreatePermision extends Seeder {

	/**
	 * Run the AssignRoleSuperAdmin seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$manageUser = new Permission;
		$manageUser->name = 'manage_user';
		$manageUser->display_name = 'Manage User';
		$manageUser->save();

		$manageMenus = new Permission;
		$manageMenus->name = 'manage_menus';
		$manageMenus->display_name = 'Manage Menus';
		$manageMenus->save();

		$manageTransactoin = new Permission;
		$manageTransactoin->name = 'manage_transaction';
		$manageTransactoin->display_name = 'Manage Transaction';
		$manageTransactoin->save();

		$manageKitcen = new Permission;
		$manageKitcen->name = 'manage_kitchen';
		$manageKitcen->display_name = 'Manage kitcen';
		$manageKitcen->save();

		$manageOrder = new Permission;
		$manageOrder->name = 'manage_order';
		$manageOrder->display_name = 'Manage Order';
		$manageOrder->save();		

		$manageAll = new Permission;
		$manageAll->name = 'manage_all';
		$manageAll->display_name = 'Manage All';
		$manageAll->save();									
	}

}

class AssignRoleSuperAdmin extends Seeder {

	/**
	 * Run the AssignRoleSuperAdmin seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$roleSuperAdmin = Role::where('name', '=', 'SuperAdmin')->first();
		$superAdmin = User::where('username', '=', 'admin')->first();
		$superAdmin->attachRole($roleSuperAdmin);
	}

}

class AddPermissionRole extends Seeder {

	/**
	 * Run the AddPermissionSuperAdmin seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$manageAll = Permission::where('name', '=', 'manage_all')->first();
		$superAdmin = Role::where('name' , '=', 'SuperAdmin')->first();
		$superAdmin->perms()->sync(array($manageAll->id));

		$manageAll = Permission::where('name', '=', 'manage_transaction')->first();
		$superAdmin = Role::where('name' , '=', 'Cashier')->first();
		$superAdmin->perms()->sync(array($manageAll->id));

		$manageAll = Permission::where('name', '=', 'manage_kitchen')->first();
		$superAdmin = Role::where('name' , '=', 'Chef')->first();
		$superAdmin->perms()->sync(array($manageAll->id));

		$manageAll = Permission::where('name', '=', 'manage_order')->first();
		$superAdmin = Role::where('name' , '=', 'Writers')->first();
		$superAdmin->perms()->sync(array($manageAll->id));			
	}

}


