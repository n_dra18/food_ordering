<?php

class SeedSettings extends Seeder {

	/**
	 * Run the MejaTableSeeder seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$dataSettings = [
			['key' => 'resto_name', 'values' => 'Not Set', 'created_at' => new DateTime, 'updated_at' => new DateTime ],
			['key' => 'resto_address', 'values' => 'Not Set', 'created_at' => new DateTime, 'updated_at' => new DateTime ],
			['key' => 'resto_tlp', 'values' => 'Not Set', 'created_at' => new DateTime, 'updated_at' => new DateTime ],
			['key' => 'resto_logo_path', 'values' => 'Not Set', 'created_at' => new DateTime, 'updated_at' => new DateTime ],
			['key' => 'dest_print_chasier', 'values' => 'Not Set', 'created_at' => new DateTime, 'updated_at' => new DateTime ],
			['key' => 'dest_print_report', 'values' => 'Not Set', 'created_at' => new DateTime, 'updated_at' => new DateTime ],
		];
		DB::table('settings')->insert($dataSettings);	
	}

}
