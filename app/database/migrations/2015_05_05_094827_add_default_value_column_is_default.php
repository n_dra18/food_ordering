<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValueColumnIsDefault extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("ALTER TABLE  `meja` CHANGE  `is_deleted`  `is_deleted` TINYINT( 4 ) NOT NULL DEFAULT  '0';");
		DB::statement("ALTER TABLE  `menus` CHANGE  `is_deleted`  `is_deleted` TINYINT( 4 ) NOT NULL DEFAULT  '0';");
		DB::statement("ALTER TABLE  `users` CHANGE  `is_deleted`  `is_deleted` TINYINT( 4 ) NOT NULL DEFAULT  '0';");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
