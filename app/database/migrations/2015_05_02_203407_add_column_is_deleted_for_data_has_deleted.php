<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsDeletedForDataHasDeleted extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($table)
		{
			$table->tinyInteger('is_deleted');
		});

		Schema::table('menus', function($table)
		{
			$table->tinyInteger('is_deleted');
		});

		Schema::table('meja', function($table)
		{
			$table->tinyInteger('is_deleted');
		});				
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
