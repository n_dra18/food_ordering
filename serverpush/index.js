var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

io.on('connection', function(socket){
  console.log('a user connected');

  // socket.on('disconnect', function(){
  //   console.log('user disconnected');
  // });

  socket.on('push notif', function(msg){
    io.emit('recieve notif', msg);
  });  

});


http.listen(8080, function(){
  console.log('listening on *:8080');
});